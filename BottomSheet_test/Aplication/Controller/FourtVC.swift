//
//  FourtVC.swift
//  BottomSheet_test
//
//  Created by Владислав Мурыгин on 23.12.2020.
//

import UIKit

class FourtVC: UIViewController {
    
    let simpleButton: UIButton = {
        let button = UIButton()
        let screen = UIScreen.main.bounds
        button.layer.cornerRadius = 10
        button.setTitle("Кейс 1", for: .normal)
        button.frame = CGRect(x: screen.size.width / 4 , y: 100 , width: screen.size.width / 2 , height: 30)
        button.addTarget(self, action: #selector(simpleButtonAction), for: .touchUpInside)
        button.backgroundColor = .blue
        return button
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .gray
        view.addSubview(simpleButton)
        title = "Кейс 4"
        
        let backButton = UIButton(type: .custom)
        backButton.setTitle("Назад", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal)
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    
    @objc func simpleButtonAction(sender: UIButton!){
        let vc = FirstVC()
        let navContrl = UINavigationController(rootViewController: vc)
        
        var options = SheetOptions()
        options.useFullScreenMode = true
        options.shrinkPresentingViewController = false
        
        let sheet = SheetViewController(controller: navContrl, sizes: [.fixed(200), .fullscreen], options: options)
        sheet.cornerRadius = 20

        present(sheet, animated: true, completion: nil)
             
    }
    
    @objc func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }

}
