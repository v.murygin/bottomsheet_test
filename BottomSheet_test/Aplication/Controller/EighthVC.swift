//
//  EighthVC.swift
//  BottomSheet_test
//
//  Created by Владислав Мурыгин on 25.12.2020.
//

import UIKit

class EighthVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .green
        title = "Кейс 8"
        
        let backButton = UIButton(type: .custom)
        backButton.setTitle("Назад", for: .normal)
        backButton.setTitleColor(backButton.tintColor, for: .normal)
        backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)

    }
    
    @objc func backAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        
    }
    
}
